# storage/block/bz2180358_hang_blk_mq_get_tag

Storage: fio hangs on scsi_debug in blk_mq_get_tag

## How to run it
Please refer to the top-level README.md for common dependencies.

### Install dependencies
```bash
root# bash ../../../cki_bin/pkgs_install.sh metadata
```

### Execute the test
```bash
bash ./runtest.sh
```
